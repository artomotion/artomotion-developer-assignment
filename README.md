# Artomotion developer assignment
### Ball trajectory tracking

In this assignment, you're going to implement a ball trajectory tracker: 
- There is **no limitation** on **what language or framework** you're going to use to implement this app. You can build in any form such as a web app, a mobile app, a native OS app etc.
- Your app must have **a simple GUI** for users to utilize and display the final result.
- Feature requests:
	1. Provide a button on your UI for user to select a video that they want to analyze.  (The input video is like the example.avi in the repo)
	2. Provide a canvas on your UI to play the video with your analyzed result.
	3. The final result does not need to be fancy, but at least, indicating the baseball location in each frame.
- Follow the **standard Git procedures** to branch off the master branch, as well as create a **PR (pull request/merge request)** when it's ready
- Please give **a brief introduction** about your app (like how to execute it) in your README and submit with your PR.
- Maintain the PR in a smaller size (remove unrelated, local, and auto generated files) in order to make it easier to review
- Git practices, PR readability, architecture + design, concise code, and logic will be taken into consideration
- UI design (i.e. whether the app is pretty or not) is not important (not taken into consideration). UX / Useability is.

The most important things:
- **Must create a PR before deadline, no matter you have finished the assignment or not. Otherwise, we will consider you dropped out from this interview process.**
- **You can use any source that you find on Internet, but please cite where you find it. Otherwise, we will consider as 
plagiarism**

Hint:
- **Only few objects change in a sequence of frames, maybe you can do image diff to find the moving objects.**
- **AI is also a possible way to achieve.**

Here are some useful links for you to finish this assignment:
- [Gitlab creating merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [Git tutorial](https://zlargon.gitbooks.io/git-tutorial/content/)
- [連猴子都能懂的Git入門指南](https://backlog.com/git-tutorial/tw/)
- [30 天精通 Git 版本控管](https://github.com/doggy8088/Learn-Git-in-30-days/blob/master/zh-tw/README.md)
- [How to Write a Git Commit Message](https://cbea.ms/git-commit/)